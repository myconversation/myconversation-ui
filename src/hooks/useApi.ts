import axios, { AxiosError, AxiosRequestConfig } from 'axios'

export const instance = axios.create()
//   @ts-ignore
export const API_URL = 'https://appsvc-myconversation.azurewebsites.net/'
export const initializeBaseURL = () => {
  // @ts-ignore
  instance.defaults.baseURL = API_URL
}

export const initializeToken = () => {
  instance.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
  instance.defaults.headers.common['Access-Control-Allow-Methods'] =
    'GET,PUT,POST,DELETE,PATCH,OPTIONS'
}

const handleError = (
  error: AxiosError,
  handler: ((error: AxiosError) => void) | undefined,
  defaultHandler: (error: AxiosError) => void
) => {
  if (handler) {
    handler(error)
  } else {
    defaultHandler(error)
  }
}

const handleResponseError = (
  error: AxiosError,
  statusCode: number,
  handler: ((error: AxiosError) => void) | undefined,
  defaultHandler: (error: AxiosError) => void
) => {
  const status = error?.response?.status
  if (status && status >= statusCode) {
    handleError(error, handler, defaultHandler)
    return true
  }
  return false
}

export type UseFetchConfig = {
  failQuietly?: boolean
  on500Error?: (error: AxiosError) => void
  on400Error?: (error: AxiosError) => void
  onRequestError?: (error: AxiosError) => void
  onUnhandledError?: (error: AxiosError) => void
}
export const useApi = (useFetchConfig?: UseFetchConfig) => {
  initializeBaseURL()

  const handleFetchError = <T>(error: AxiosError) => {
    const defaultHandler = (error: AxiosError) => {
      if (!useFetchConfig?.failQuietly) {
        console.error(`${error.message}`)
      }
    }
    
    if (error.response) {
      // response was created
      let caught = false
      if (!caught) {
        caught = handleResponseError(error, 500, useFetchConfig?.on500Error, defaultHandler)
      }
      if (!caught) {
        caught = handleResponseError(error, 400, useFetchConfig?.on400Error, defaultHandler)
      }
    } else if (error.request) {
      // request was created
      handleError(error, useFetchConfig?.onRequestError, defaultHandler)
    } else {
      // something else happened
      handleError(error, useFetchConfig?.onUnhandledError, defaultHandler)
    }

    // throw new error to be caught by react-query
    throw error
  }

  const post = async <T = any, D = any>(uri: string, data: D, config: AxiosRequestConfig = {}) => {
    initializeToken()
    try {
      const response = await instance.post<T>(uri, data, config)
      return response.data
    } catch (error: any) {
      return handleFetchError<T>(error)
    }
  }

  const get = async <T = any>(uri: string, config: AxiosRequestConfig = {}) => {
    initializeToken()
    try {
      const response = await instance.get<T>(uri, config)
      return response.data
    } catch (error: any) {
      handleFetchError<T>(error)
    }
  }

  const put = async <T = any, D = any>(uri: string, data: D, config: AxiosRequestConfig = {}) => {
    initializeToken()
    try {
      const response = await instance.put<T>(uri, data, config)
      return response.data
    } catch (error: any) {
      return handleFetchError<T>(error)
    }
  }

  // delete
  // request

  return { get, post, put }
}
