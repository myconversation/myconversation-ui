import * as React from "react"
import {
  ChakraProvider,
  Box,
  VStack,
  Grid,
  theme,
} from "@chakra-ui/react"
import { AppRoutes } from "./routes"

export const App = () => (
  <ChakraProvider theme={theme}>
    <Box textAlign="center" fontSize="xl" bgColor="teal.200">
      <Grid minH="100vh" p={3}>
        <VStack>
          <AppRoutes />
        </VStack>
      </Grid>
    </Box>
  </ChakraProvider>
)
