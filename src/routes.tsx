import { Routes, Route } from "react-router-dom"
import { Conversation, Conversations } from "./domain"

export const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Conversations />} />
      <Route path="/:id" element={<Conversation />} />
    </Routes>
  )
}