export type Conversation = {
  id: number,
  title: string,
  createdDate: Date
}