import { useQuery } from 'react-query'
import { useApi } from '../../../hooks/useApi'
import { Conversation } from '../../../types/Conversation'

const GET_CONVERSATION = 'get-conversation'

export const useConversation = (id: number | string) => {
  const { get } = useApi()

  return useQuery<Conversation>(GET_CONVERSATION, async () => {
    return await get(`/api/Conversations/${id}`)
  }) 
}