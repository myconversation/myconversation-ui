import { useMutation, useQuery, useQueryClient } from 'react-query'
import { useApi } from '../../../hooks/useApi'
import { Message } from '../../../types/Message'

const GET_CONVERSATION_MESSAGES = 'get-conversation-messages'
const POST_CONVERSATION_MESSAGE = 'post-conversation-message'

export const useMessages = (id: number | string) => {
  const { get } = useApi()

  return useQuery<Message[]>(GET_CONVERSATION_MESSAGES, async () => {
    return await get(`/api/Conversations/${id}/Messages`)
  })
}

export const usePostMessage = () => {
  const { post } = useApi()
  const queryClient = useQueryClient()
  
  const mutation = useMutation(POST_CONVERSATION_MESSAGE,
    async (data: { conversationId: number | string; text: string }) => {
      return await post(`/api/Conversations/${data.conversationId}/Messages`, data)},
  {
    onSettled: () => {
      queryClient.invalidateQueries(GET_CONVERSATION_MESSAGES)
    }
  })

  return mutation
}

