import { Box, Breadcrumb, BreadcrumbItem, BreadcrumbLink, Divider, Text, HStack, Input, Button  } from "@chakra-ui/react"
import { FC, useState } from "react"
import { Link, useParams } from "react-router-dom";
import { useConversation } from "./hooks/useConversation"
import { useMessages, usePostMessage } from "./hooks/useMessages";

export const Conversation: FC = () => {
  const [text, setText] = useState('')
  const handleTextChange = (event: { target: { value: any; }; }) => setText(event.target.value)

  const params = useParams();
  const query = useConversation(params?.id ?? '')
  const isLoading = query.isLoading;
  const data = query?.data

  const { mutate } = usePostMessage()

  const handleTextSubmit = (id: number) => {
    const data = {
      text: text,
      conversationId: id
    }

    console.log(data)

    mutate(data)

    setText('')
  }
  
  return (
    <Box width='lg' height='lg' shadow='lg' bgColor='gray.200' border='1px' borderRadius='lg' >
      <Box paddingTop={4} paddingLeft={4} paddingRight={4} paddingBottom={2} display='flex' alignItems='center' justifyContent='space-between'>
        <Breadcrumb>
          <BreadcrumbItem>
            <BreadcrumbLink as={Link} to='/'>Conversation</BreadcrumbLink>
          </BreadcrumbItem>
          <BreadcrumbItem>
            <BreadcrumbLink href='#'>{data?.title}</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
      </Box>
      <Divider colorScheme='teal' />
      <Box minHeight='75%' maxHeight='75%'>{/* TODO: Need to make this area scrollable when messages are long */}
        {data?.id != null && !isLoading ? (
          <Messages id={data?.id} />
        ) : null}
      </Box>
      <Divider colorScheme='teal' />
      <Box padding={2}>
        <HStack>
          <Input placeholder='Type Message' size='sm' variant='outline' value={text} onChange={handleTextChange} />
          <Button colorScheme='teal' onClick={() => handleTextSubmit(Number(params?.id) ?? 0)}>Send</Button>
        </HStack>
      </Box>
    </Box>
  )
}

const Messages: FC<{ id: number; }> = (props) => {
  const { id } = props
  const query = useMessages(id)
  const data = query.data || []
  const isLoading = query.isLoading

  return (
    <Box>
      {data.length <= 0 && !isLoading ? (
        <Box padding={2}>
          <Text fontSize='md'>No Messages</Text>
        </Box>
      ) : null}
      {data.map(value => {
        return (
          <Box key={`message-${value.id}`} padding={2} textAlign={value.id % 2 === 0 ? 'right' : 'left'}>
            <Text fontSize='md'>{value.text}</Text>
          </Box>
        )
      })}
    </Box>
  )
}