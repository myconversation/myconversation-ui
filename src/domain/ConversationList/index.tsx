import { Box, Heading, Button, Divider, TableContainer, Table, Thead, Tr, Th, Tbody, Link  } from "@chakra-ui/react"
import { FC } from "react"
import { useConversations } from "./hooks/useConversations"
import moment from "moment"
import { Link as RouterLink } from "react-router-dom"

export const Conversations: FC = () => {
  const query = useConversations()
  const data = query?.data || []

  return (
    <Box width='lg' height='lg' shadow='lg' bgColor='gray.200' border='1px' borderRadius='lg' >
      <Box paddingTop={4} paddingLeft={4} paddingRight={4} paddingBottom={2} display='flex' alignItems='center' justifyContent='space-between'>
        <Heading size='lg'>Conversations</Heading>
        <Button colorScheme='teal'>New</Button>
      </Box>
      <Divider colorScheme='teal' />
      {}
      <TableContainer>
        <Table variant='simple'>
          <Thead>
            <Tr>
              <Th>Title</Th>
              <Th>Created Date</Th>
            </Tr>
          </Thead>
          <Tbody>
            {data.map((element) => {
              return (
                <Tr key={`conversation-list-${element.id}`}>
                  <Th><Link as={RouterLink} to={`/${element.id}`}>{element.title}</Link></Th>
                  <Th>{moment(moment.utc(element.createdDate).toDate()).local().format('YYYY-MM-DD hh:mm:ss a')}</Th>
                </Tr>
              )
            })}
          </Tbody>
        </Table>
      </TableContainer>
    </Box>
  )
}