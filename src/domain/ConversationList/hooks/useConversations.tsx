import { useQuery } from 'react-query'
import { useApi } from '../../../hooks/useApi'
import { Conversation } from '../../../types/Conversation'

const GET_CONVERSATIONS = 'get-conversations'

export const useConversations = () => {
  const { get } = useApi()

  return useQuery<Conversation[]>(GET_CONVERSATIONS, async () => {
    return await get('/api/Conversations')
  }) 
}